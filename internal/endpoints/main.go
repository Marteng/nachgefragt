package endpoints

import (
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-chi/chi/v5"
)

func MainRouter() chi.Router {
	router := chi.NewRouter()
	router.Get("/", landingPage)
	router.Route("/questions", questionRouter)
	router.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		workDir, _ := os.Getwd()
		filesDir := http.Dir(filepath.Join(workDir, "static"))
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(filesDir))
		fs.ServeHTTP(w, r)
	})
	return router
}

func landingPage(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World"))
}
