.PHONY: run install fmt gofumpt

run: install fmt
	go run .
install: go.sum
go.sum:
	go get
fmt: gofumpt
	go fmt
	-gofumpt -extra -w .
gofumpt:
	go install mvdan.cc/gofumpt@latest
